package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;

public class Network<T> {
	private List<Layer> layers = new ArrayList<>();

	public Network(int[] layerStructure, double learningRate,
			DoubleUnaryOperator activationFunction, DoubleUnaryOperator derivativeActivationFunction) {
		if (layerStructure.length < 3) {
			throw new IllegalArgumentException("Error: Should be at least 3 layers (1 input, 1 hidden, 1 output).");
		}
		// input layer
		var inputLayer = new Layer(Optional.empty(), layerStructure[0], learningRate, activationFunction,
				derivativeActivationFunction);
		layers.add(inputLayer);
		// hidden layers and output layer
		for (var i = 1; i < layerStructure.length; i++) {
			var nextLayer = new Layer(Optional.of(layers.get(i - 1)), layerStructure[i], learningRate,
					activationFunction,
					derivativeActivationFunction);
			layers.add(nextLayer);
		}
	}

	/**
	 * Pushes input data to the first layer, then output from the first
	 * as input to the second, second to the third, etc.
	 * @param input
	 * @return
	 */
	private double[] outputs(double[] input) {
		double[] result = input;
		for (Layer layer : layers) {
			result = layer.outputs(result);
		}
		return result;

	}



	/**
	 * Figure out each neuron's changes based on the errors of the output
	 * 	 versus the expected outcome
	 * @param expected
	 */
	private void backpropagate(double[] expected) {
		// calculate delta for output layer neurons
		int lastLayer = layers.size() - 1;
		layers.get(lastLayer).calculateDeltasForOutputLayer(expected);
		// calculate delta for hidden layers in reverse order
		for (int i = lastLayer - 1; i >= 0; i--) {
			layers.get(i).calculateDeltasForHiddenLayer(layers.get(i + 1));
		}
	}

	/**
	 * backpropagate() doesn't actually change any weights
	 * 	 this function uses the deltas calculated in backpropagate() to
	 * 	 actually make changes to the weights
	 */
	private void updateWeights() {
		for (Layer layer : layers.subList(1, layers.size())) {
			for (Neuron neuron : layer.getNeurons()) {
				for (var w = 0; w < neuron.getWeights().length; w++) {
					neuron.weights[w] = neuron.getWeights()[w] + (neuron.learningRate *
							layer.getPreviousLayer().get().getOutputCache()[w] * neuron.getDelta());
				}
			}
		}
	}

	/**
	 * train() uses the results of outputs() run over many inputs and compared
	 * 	 against expecteds to feed backpropagate() and updateWeights()
	 * @param inputs
	 * @param expecteds
	 */
	public void train(List<double[]> inputs, List<double[]> expecteds) {
		for (var i = 0; i < inputs.size(); i++) {
			double[] xs = inputs.get(i);
			double[] ys = expecteds.get(i);
			outputs(xs);
			backpropagate(ys);
			updateWeights();
		}
	}

	public class Results {
		public final int correct;
		public final int trials;
		public final double percentage;

		public Results(int correct, int trials, double percentage) {
			this.correct = correct;
			this.trials = trials;
			this.percentage = percentage;
		}
	}

	/**
	 * for generalized results that require classification
	 * 	 this function will return the correct number of trials
	 * 	 and the percentage correct out of the total
	 * @param inputs
	 * @param expecteds
	 * @param interpret
	 * @return
	 */
	public Results validate(List<double[]> inputs, List<T> expecteds, Function<double[], T> interpret) {
		var correct = 0;
		for (var i = 0; i < inputs.size(); i++) {
			double[] input = inputs.get(i);
			var expected = expecteds.get(i);
			var result = interpret.apply(outputs(input));
			if (result.equals(expected)) {
				correct++;
			}
		}
		double percentage = (double) correct / (double) inputs.size();
		return new Results(correct, inputs.size(), percentage);
	}
}
