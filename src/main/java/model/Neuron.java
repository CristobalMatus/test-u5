package model;

import util.Util;

import java.util.function.DoubleUnaryOperator;

public class Neuron {
	public double[] weights;
	public final double learningRate;
	private double outputCache;
	private double delta;
	public final DoubleUnaryOperator activationFunction;
	public final DoubleUnaryOperator derivativeActivationFunction;

	public Neuron(double[] weights, double learningRate, DoubleUnaryOperator activationFunction,
			DoubleUnaryOperator derivativeActivationFunction) {
		this.weights = weights;
		this.learningRate = learningRate;
		outputCache = 0.0;
		delta = 0.0;
		this.activationFunction = activationFunction;
		this.derivativeActivationFunction = derivativeActivationFunction;
	}

	public double[] getWeights() {
		return weights;
	}

	public double getOutputCache() {
		return outputCache;
	}

	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		this.delta = delta;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public double output(double[] inputs) {
		outputCache = Util.dotProduct(inputs, weights);
		return activationFunction.applyAsDouble(outputCache);
	}

}
