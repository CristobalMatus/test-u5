package app;

import service.Wine;
import java.util.logging.Logger;

public class App {

    public static void main(String[] args) {
        var wineTest = new Wine();
        var results = wineTest.classify();
        logger.info(results.correct + " correct of " + results.trials + " = " +
                results.percentage * 100 + "%");
    }

    private static final Logger logger = Logger.getLogger(Wine.class.getName());
}
